def test_epel_release_installed(host):
    epel_release_repository = host.package('epel-release')

    assert epel_release_repository.is_installed
