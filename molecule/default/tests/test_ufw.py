import pytest


def test_ufw_installed(host):
    ufw_package = host.package('ufw')

    assert ufw_package.is_installed


def test_ufw_is_running(host):
    ufw_service = host.service('ufw')

    assert ufw_service.is_running
    assert ufw_service.is_enabled


def test_ufw_policy_set_to_deny(host):
    expected_output = 'deny (incoming)'
    with host.sudo():
        output = host.check_output('ufw status verbose')
        assert expected_output in output


@pytest.mark.parametrize(
    'port', (
        443,
        48001,
        80,
    )
)
def test_ufw_allow_open_port(port, host):
    expected_output = 'ufw allow {}'.format(port)
    with host.sudo():
        output = host.check_output('ufw show added')
        assert expected_output in output
