[![pipeline status](https://git.coop/aptivate/ansible-roles/ufw/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/ufw/commits/master)

# ufw

A role to install and configure UFW.

# Requirements

None.

# Role Variables

  * `ufw_open_ports`: Ports to open on the firewall.
    * See [defaults/main.yml](defaults/main.yml) for defaults.

# Dependencies

None.

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: ufw
```

# Testing

We use Linode for our testing here.

You'll need to expose the `LINODE_API_KEY` environment variable for that.

```
$ pipenv install --dev
$ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
